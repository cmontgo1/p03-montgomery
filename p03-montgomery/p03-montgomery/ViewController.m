//
//  ViewController.m
//  p03-montgomery
//
//  Created by Montgomery Chelsea on 2/11/16.
//  Copyright © 2016 Montgomery Chelsea. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

@synthesize gs;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    //Create the gamescreen
    [gs createPlayField];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
