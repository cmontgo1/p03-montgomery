//
//  ViewController.h
//  p03-montgomery
//
//  Created by Montgomery Chelsea on 2/11/16.
//  Copyright © 2016 Montgomery Chelsea. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GameScreen.h"

@interface ViewController : UIViewController

//Give the ViewController access to the GameScreen
@property(nonatomic,strong) IBOutlet GameScreen *gs;


@end

