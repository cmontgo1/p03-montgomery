//
//  GameScreen.h
//  p03-montgomery
//
//  Created by Montgomery Chelsea on 2/11/16.
//  Copyright © 2016 Montgomery Chelsea. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GameScreen : UIView
{
    float dx, dy; //Stand for ball movement
}

//Create a view for the ball, paddle, and create a timer
@property(nonatomic, strong) UIView *paddle;
@property(nonatomic, strong) UIView *ball;
@property(nonatomic, strong) NSTimer *timer;

//Score label
@property(nonatomic, strong) UILabel *score;

//You won label
@property(nonatomic, strong) UILabel *won;

//You lose label
@property(nonatomic, strong) UILabel *lose;

//Function to display field, ball, etc
-(void)createPlayField;

@end
