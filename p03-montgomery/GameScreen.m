//
//  GameScreen.m
//  p03-montgomery
//
//  Created by Montgomery Chelsea on 2/11/16.
//  Copyright © 2016 Montgomery Chelsea. All rights reserved.
//

#import "GameScreen.h"

@implementation GameScreen

//Code needed to use paddle, ball, timer
@synthesize paddle, ball;
@synthesize timer;
@synthesize score;
@synthesize won;
@synthesize lose;

//Array to hold "bricks"
NSMutableArray *bricks;

//Number of bricks to draw
int num_bricks = 60;

//Size of bricks
int x_size = 30;
int y_size = 10;

//Ongoing score
int cur_score = 0;

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)createPlayField
{
    paddle = [[UIView alloc] initWithFrame:CGRectMake(140, 575, 90, 25)];
    [self addSubview:paddle];
    [paddle setBackgroundColor:[UIColor blackColor]];
    
    ball = [[UIView alloc] initWithFrame:CGRectMake(180, 551, 10, 10)];
    [self addSubview:ball];
    [ball setBackgroundColor:[UIColor redColor]];
    
    //Set the initial speed
    dx = 3;
    dy = 3;
    
    //Create the bricks
    bricks = [NSMutableArray arrayWithObjects:nil];
    
    int x_placement = 10;
    int y_placement = 60;
    
    for(int i = 0; i<num_bricks; ++i)
    {
        UIView *new_brick = [[UIView alloc] initWithFrame:CGRectMake(x_placement, y_placement, x_size, y_size)];
        
        [self addSubview:new_brick];
        
        [new_brick setBackgroundColor:[UIColor lightGrayColor]];
        
        [bricks addObject:new_brick];
        
        x_placement += x_size + 6;
        
        if((i+1)%10==0 && i!=0)
        {
            y_placement += y_size + 6;
            x_placement = 10;
        }
        
    }
    
    //Set the score label
    score = [[UILabel alloc] initWithFrame:CGRectMake(150, 10, 90, 30)];
    [score setText:@"Score: 0"];
    [score setTextColor:[UIColor blueColor]];
    [self addSubview:score];
    
    won = [[UILabel alloc] initWithFrame:CGRectMake(120, 130, 150, 180)];
    [won setText:@"YOU WON!"];
    [won setFont:[UIFont boldSystemFontOfSize:24]];
    [won setTextColor:[UIColor blueColor]];
    [self addSubview:won];
    [won setHidden:YES];
    
    lose = [[UILabel alloc] initWithFrame:CGRectMake(120, 130, 150, 180)];
    [lose setText:@"YOU LOSE!"];
    [lose setFont:[UIFont boldSystemFontOfSize:24]];
    [lose setTextColor:[UIColor blueColor]];
    [self addSubview:lose];
    [lose setHidden:YES];
    
}

//Function to reset the game
-(IBAction)reset:(id)sender
{
    [timer invalidate];
    
    for(int i = 0; i<num_bricks; ++i)
    {
        [bricks[i] setHidden:NO];
    }
    [ball setCenter:CGPointMake(180, 551)];
    [paddle setCenter:CGPointMake(180, 575)];
    [score setText:@"Score: 0"];
    
    cur_score = 0;
    
    [won setHidden:YES];
    [lose setHidden:YES];
}

//Functions to handle the touches we get
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    for ( UITouch *t in touches )
    {
        CGPoint p = [t locationInView:self];
        
        //set p.y to keep it from moving up or down
        p.y = 575;
        
        [paddle setCenter:p];
    }
}

-(void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self touchesBegan:touches withEvent:event];
}

//Animation functions
-(IBAction) startAnimation:(id)sender
{
    timer= [NSTimer scheduledTimerWithTimeInterval:.01 target:self selector:@selector(timerEvent:) userInfo:nil repeats:YES];
}

-(IBAction)stopAnimation:(id)sender
{
    [timer invalidate];
}

-(void)timerEvent:(id)sender
{
    CGRect bounds = [self bounds];
    
    //NSLog(@"timer event");
    
    CGPoint p = [ball center];
    
    if((p.x + dx)<0)
    {
        dx = -dx;
    }
    
    if((p.y + dy)<0)
    {
        dy = -dy;
    }
    
    if((p.x + dx)>bounds.size.width)
    {
        dx = -dx;
    }
    
    Boolean keep_checking = true;
    
    if((p.y + dy)>bounds.size.height)
    {
        dy = -dy;
        keep_checking = false;
        [lose setHidden:NO];
        [timer invalidate];
    }
    
    if(keep_checking)
    {
    
        //Move the ball
        p.x += dx;
        p.y += dy;
        [ball setCenter:p];
    
        //Paddle intersect check
        if(CGRectIntersectsRect([ball frame], [paddle frame]))
        {
            dy = -dy;
            p.y += 2*dy;
            [ball setCenter:p];
        }
    
        //Brick intersect check
        for(int i = 0; i<num_bricks; ++i)
        {
            if(CGRectIntersectsRect([ball frame], [bricks[i] frame]) && [bricks[i] isHidden]==NO)
            {
                //NSLog(@"Intersect");
            
                [bricks[i] setHidden:YES];
            
                dy = -dy;
                p.y += 2*dy;
                [ball setCenter:p];
            
                cur_score += 10;
            
                NSString *score_string = [NSString stringWithFormat:@"%d", cur_score];
            
                NSString *current_score_string = [@"Score: " stringByAppendingString:score_string];
                [score setText:current_score_string];

            }
        }
    
        //Win check
        [self did_i_win];
    }
        
}

-(void)did_i_win
{
    Boolean i_won = true;
    
    for(int i = 0; i<num_bricks; ++i)
    {
        if([bricks[i] isHidden] == NO)
        {
            i_won = false;
            break;
        }
    }
    
    if(i_won)
    {
        [timer invalidate];
        
        [won setHidden:NO];
    }
}

@end
